package ZUT.app.ZUT;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import pl.edu.zut.scopusproj.ZutApplication;

@WebMvcTest(ZutApplication.class)
@ContextConfiguration(classes={SpringBootApplication.class})
@SpringBootTest
public class ZutApplicationTests {

	@Test
	public void contextLoads() {
	}

}
