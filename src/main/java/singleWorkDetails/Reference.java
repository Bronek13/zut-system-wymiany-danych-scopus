
package singleWorkDetails;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "@id",
    "ref-info"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class Reference {

    @JsonProperty("@id")
    private String id;
    @JsonProperty("ref-info")
    private RefInfo refInfo;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("@id")
    public String getId() {
        return id;
    }

    @JsonProperty("@id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("ref-info")
    public RefInfo getRefInfo() {
        return refInfo;
    }

    @JsonProperty("ref-info")
    public void setRefInfo(RefInfo refInfo) {
        this.refInfo = refInfo;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
