
package singleWorkDetails;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "item-info",
    "head",
    "tail"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class Bibrecord {

    @JsonProperty("item-info")
    private ItemInfo itemInfo;
    @JsonProperty("head")
    private Head head;
    @JsonProperty("tail")
    private Tail tail;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("item-info")
    public ItemInfo getItemInfo() {
        return itemInfo;
    }

    @JsonProperty("item-info")
    public void setItemInfo(ItemInfo itemInfo) {
        this.itemInfo = itemInfo;
    }

    @JsonProperty("head")
    public Head getHead() {
        return head;
    }

    @JsonProperty("head")
    public void setHead(Head head) {
        this.head = head;
    }

    @JsonProperty("tail")
    public Tail getTail() {
        return tail;
    }

    @JsonProperty("tail")
    public void setTail(Tail tail) {
        this.tail = tail;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
