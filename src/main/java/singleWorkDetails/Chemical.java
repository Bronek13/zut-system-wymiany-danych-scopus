
package singleWorkDetails;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "chemical-name",
    "cas-registry-number"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class Chemical {

    @JsonProperty("chemical-name")
    private String chemicalName;
    @JsonProperty("cas-registry-number")
    private String casRegistryNumber;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("chemical-name")
    public String getChemicalName() {
        return chemicalName;
    }

    @JsonProperty("chemical-name")
    public void setChemicalName(String chemicalName) {
        this.chemicalName = chemicalName;
    }

    @JsonProperty("cas-registry-number")
    public String getCasRegistryNumber() {
        return casRegistryNumber;
    }

    @JsonProperty("cas-registry-number")
    public void setCasRegistryNumber(String casRegistryNumber) {
        this.casRegistryNumber = casRegistryNumber;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
