
package singleWorkDetails;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "@source",
    "chemical"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class Chemicals {

    @JsonProperty("@source")
    private String source;
    @JsonProperty("chemical")
    private List<Chemical> chemical = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("@source")
    public String getSource() {
        return source;
    }

    @JsonProperty("@source")
    public void setSource(String source) {
        this.source = source;
    }

    @JsonProperty("chemical")
    public List<Chemical> getChemical() {
        return chemical;
    }

    @JsonProperty("chemical")
    public void setChemical(List<Chemical> chemical) {
        this.chemical = chemical;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
