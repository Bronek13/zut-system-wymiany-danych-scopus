
package singleWorkDetails;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "itemid"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class RefdItemidlist {

    @JsonProperty("itemid")
    private Itemid_ itemid;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("itemid")
    public Itemid_ getItemid() {
        return itemid;
    }

    @JsonProperty("itemid")
    public void setItemid(Itemid_ itemid) {
        this.itemid = itemid;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
