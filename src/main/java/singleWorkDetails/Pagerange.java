
package singleWorkDetails;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "@first",
    "@last"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class Pagerange {

    @JsonProperty("@first")
    private String first;
    @JsonProperty("@last")
    private String last;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("@first")
    public String getFirst() {
        return first;
    }

    @JsonProperty("@first")
    public void setFirst(String first) {
        this.first = first;
    }

    @JsonProperty("@last")
    public String getLast() {
        return last;
    }

    @JsonProperty("@last")
    public void setLast(String last) {
        this.last = last;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
