
package singleWorkDetails;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "date-created"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class History {

    @JsonProperty("date-created")
    private DateCreated dateCreated;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("date-created")
    public DateCreated getDateCreated() {
        return dateCreated;
    }

    @JsonProperty("date-created")
    public void setDateCreated(DateCreated dateCreated) {
        this.dateCreated = dateCreated;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
