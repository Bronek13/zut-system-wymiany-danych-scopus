
package singleWorkDetails;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "prism:url",
    "dc:identifier",
    "pubmed-id",
    "pii",
    "prism:doi",
    "dc:title",
    "prism:aggregationType",
    "srctype",
    "citedby-count",
    "prism:publicationName",
    "prism:issn",
    "prism:volume",
    "prism:issueIdentifier",
    "prism:startingPage",
    "prism:endingPage",
    "prism:pageRange",
    "prism:coverDate",
    "dc:creator",
    "dc:description",
    "intid",
    "link"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class Coredata {

    @JsonProperty("prism:url")
    private String prismUrl;
    @JsonProperty("dc:identifier")
    private String dcIdentifier;
    @JsonProperty("pubmed-id")
    private String pubmedId;
    @JsonProperty("pii")
    private String pii;
    @JsonProperty("prism:doi")
    private String prismDoi;
    @JsonProperty("dc:title")
    private String dcTitle;
    @JsonProperty("prism:aggregationType")
    private String prismAggregationType;
    @JsonProperty("srctype")
    private String srctype;
    @JsonProperty("citedby-count")
    private String citedbyCount;
    @JsonProperty("prism:publicationName")
    private String prismPublicationName;
    @JsonProperty("prism:issn")
    private String prismIssn;
    @JsonProperty("prism:volume")
    private String prismVolume;
    @JsonProperty("prism:issueIdentifier")
    private String prismIssueIdentifier;
    @JsonProperty("prism:startingPage")
    private String prismStartingPage;
    @JsonProperty("prism:endingPage")
    private String prismEndingPage;
    @JsonProperty("prism:pageRange")
    private String prismPageRange;
    @JsonProperty("article-number")
    private String articleNumber;
    @JsonProperty("prism:coverDate")
    private String prismCoverDate;
    @JsonProperty("dc:creator")
    private DcCreator dcCreator;
    @JsonProperty("dc:description")
    private String dcDescription;
    @JsonProperty("intid")
    private String intid;
    @JsonProperty("link")
    private List<Link> link = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("prism:url")
    public String getPrismUrl() {
        return prismUrl;
    }

    @JsonProperty("prism:url")
    public void setPrismUrl(String prismUrl) {
        this.prismUrl = prismUrl;
    }

    @JsonProperty("dc:identifier")
    public String getDcIdentifier() {
        return dcIdentifier;
    }

    @JsonProperty("dc:identifier")
    public void setDcIdentifier(String dcIdentifier) {
        this.dcIdentifier = dcIdentifier;
    }

    @JsonProperty("pubmed-id")
    public String getPubmedId() {
        return pubmedId;
    }

    @JsonProperty("pubmed-id")
    public void setPubmedId(String pubmedId) {
        this.pubmedId = pubmedId;
    }

    @JsonProperty("pii")
    public String getPii() {
        return pii;
    }

    @JsonProperty("pii")
    public void setPii(String pii) {
        this.pii = pii;
    }

    @JsonProperty("prism:doi")
    public String getPrismDoi() {
        return prismDoi;
    }

    @JsonProperty("prism:doi")
    public void setPrismDoi(String prismDoi) {
        this.prismDoi = prismDoi;
    }

    @JsonProperty("dc:title")
    public String getDcTitle() {
        return dcTitle;
    }

    @JsonProperty("dc:title")
    public void setDcTitle(String dcTitle) {
        this.dcTitle = dcTitle;
    }

    @JsonProperty("prism:aggregationType")
    public String getPrismAggregationType() {
        return prismAggregationType;
    }

    @JsonProperty("prism:aggregationType")
    public void setPrismAggregationType(String prismAggregationType) {
        this.prismAggregationType = prismAggregationType;
    }

    @JsonProperty("srctype")
    public String getSrctype() {
        return srctype;
    }

    @JsonProperty("srctype")
    public void setSrctype(String srctype) {
        this.srctype = srctype;
    }

    @JsonProperty("citedby-count")
    public String getCitedbyCount() {
        return citedbyCount;
    }

    @JsonProperty("citedby-count")
    public void setCitedbyCount(String citedbyCount) {
        this.citedbyCount = citedbyCount;
    }

    @JsonProperty("prism:publicationName")
    public String getPrismPublicationName() {
        return prismPublicationName;
    }

    @JsonProperty("prism:publicationName")
    public void setPrismPublicationName(String prismPublicationName) {
        this.prismPublicationName = prismPublicationName;
    }

    @JsonProperty("prism:issn")
    public String getPrismIssn() {
        return prismIssn;
    }

    @JsonProperty("prism:issn")
    public void setPrismIssn(String prismIssn) {
        this.prismIssn = prismIssn;
    }

    @JsonProperty("prism:volume")
    public String getPrismVolume() {
        return prismVolume;
    }

    @JsonProperty("prism:volume")
    public void setPrismVolume(String prismVolume) {
        this.prismVolume = prismVolume;
    }

    @JsonProperty("prism:issueIdentifier")
    public String getPrismIssueIdentifier() {
        return prismIssueIdentifier;
    }

    @JsonProperty("prism:issueIdentifier")
    public void setPrismIssueIdentifier(String prismIssueIdentifier) {
        this.prismIssueIdentifier = prismIssueIdentifier;
    }

    @JsonProperty("prism:startingPage")
    public String getPrismStartingPage() {
        return prismStartingPage;
    }

    @JsonProperty("prism:startingPage")
    public void setPrismStartingPage(String prismStartingPage) {
        this.prismStartingPage = prismStartingPage;
    }

    @JsonProperty("prism:endingPage")
    public String getPrismEndingPage() {
        return prismEndingPage;
    }

    @JsonProperty("prism:endingPage")
    public void setPrismEndingPage(String prismEndingPage) {
        this.prismEndingPage = prismEndingPage;
    }

    @JsonProperty("prism:pageRange")
    public String getPrismPageRange() {
        return prismPageRange;
    }

    @JsonProperty("prism:pageRange")
    public void setPrismPageRange(String prismPageRange) {
        this.prismPageRange = prismPageRange;
    }

    @JsonProperty("article-number")
    public String getArticleNumber() {
        return articleNumber;
    }

    @JsonProperty("article-number")
    public void setArticleNumber(String prismPageRange) {
        this.articleNumber = articleNumber;
    }

    @JsonProperty("prism:coverDate")
    public String getPrismCoverDate() {
        return prismCoverDate;
    }

    @JsonProperty("prism:coverDate")
    public void setPrismCoverDate(String prismCoverDate) {
        this.prismCoverDate = prismCoverDate;
    }

    @JsonProperty("dc:creator")
    public DcCreator getDcCreator() {
        return dcCreator;
    }

    @JsonProperty("dc:creator")
    public void setDcCreator(DcCreator dcCreator) {
        this.dcCreator = dcCreator;
    }

    @JsonProperty("dc:description")
    public String getDcDescription() {
        return dcDescription;
    }

    @JsonProperty("dc:description")
    public void setDcDescription(String dcDescription) {
        this.dcDescription = dcDescription;
    }

    @JsonProperty("intid")
    public String getIntid() {
        return intid;
    }

    @JsonProperty("intid")
    public void setIntid(String intid) {
        this.intid = intid;
    }

    @JsonProperty("link")
    public List<Link> getLink() {
        return link;
    }

    @JsonProperty("link")
    public void setLink(List<Link> link) {
        this.link = link;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
