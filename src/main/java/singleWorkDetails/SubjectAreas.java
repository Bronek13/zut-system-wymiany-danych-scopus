
package singleWorkDetails;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "subject-area"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class SubjectAreas {

    @JsonProperty("subject-area")
    private List<SubjectArea> subjectArea = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("subject-area")
    public List<SubjectArea> getSubjectArea() {
        return subjectArea;
    }

    @JsonProperty("subject-area")
    public void setSubjectArea(List<SubjectArea> subjectArea) {
        this.subjectArea = subjectArea;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
