
package singleWorkDetails;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "descriptorgroup",
    "classificationgroup",
    "manufacturergroup",
    "chemicalgroup"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class Enhancement {

    @JsonProperty("descriptorgroup")
    private Descriptorgroup descriptorgroup;
    @JsonProperty("classificationgroup")
    private Classificationgroup classificationgroup;
    @JsonProperty("manufacturergroup")
    private Manufacturergroup manufacturergroup;
    @JsonProperty("chemicalgroup")
    private Chemicalgroup chemicalgroup;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("descriptorgroup")
    public Descriptorgroup getDescriptorgroup() {
        return descriptorgroup;
    }

    @JsonProperty("descriptorgroup")
    public void setDescriptorgroup(Descriptorgroup descriptorgroup) {
        this.descriptorgroup = descriptorgroup;
    }

    @JsonProperty("classificationgroup")
    public Classificationgroup getClassificationgroup() {
        return classificationgroup;
    }

    @JsonProperty("classificationgroup")
    public void setClassificationgroup(Classificationgroup classificationgroup) {
        this.classificationgroup = classificationgroup;
    }

    @JsonProperty("manufacturergroup")
    public Manufacturergroup getManufacturergroup() {
        return manufacturergroup;
    }

    @JsonProperty("manufacturergroup")
    public void setManufacturergroup(Manufacturergroup manufacturergroup) {
        this.manufacturergroup = manufacturergroup;
    }

    @JsonProperty("chemicalgroup")
    public Chemicalgroup getChemicalgroup() {
        return chemicalgroup;
    }

    @JsonProperty("chemicalgroup")
    public void setChemicalgroup(Chemicalgroup chemicalgroup) {
        this.chemicalgroup = chemicalgroup;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
