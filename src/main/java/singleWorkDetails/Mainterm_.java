
package singleWorkDetails;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "@candidate",
    "@weight",
    "$"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class Mainterm_ {

    @JsonProperty("@candidate")
    private String candidate;
    @JsonProperty("@weight")
    private String weight;
    @JsonProperty("$")
    private String $;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("@candidate")
    public String getCandidate() {
        return candidate;
    }

    @JsonProperty("@candidate")
    public void setCandidate(String candidate) {
        this.candidate = candidate;
    }

    @JsonProperty("@weight")
    public String getWeight() {
        return weight;
    }

    @JsonProperty("@weight")
    public void setWeight(String weight) {
        this.weight = weight;
    }

    @JsonProperty("$")
    public String get$() {
        return $;
    }

    @JsonProperty("$")
    public void set$(String $) {
        this.$ = $;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
