
package singleWorkDetails;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "citation-info",
    "citation-title",
    "author-group",
    "correspondence",
    "abstracts",
    "source",
    "enhancement"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class Head {

    @JsonProperty("citation-info")
    private CitationInfo citationInfo;
    @JsonProperty("citation-title")
    private String citationTitle;
    @JsonProperty("author-group")
    private List<AuthorGroup> authorGroup = null;
    @JsonProperty("correspondence")
    private Correspondence correspondence;
    @JsonProperty("abstracts")
    private String abstracts;
    @JsonProperty("source")
    private Source source;
    @JsonProperty("enhancement")
    private Enhancement enhancement;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("citation-info")
    public CitationInfo getCitationInfo() {
        return citationInfo;
    }

    @JsonProperty("citation-info")
    public void setCitationInfo(CitationInfo citationInfo) {
        this.citationInfo = citationInfo;
    }

    @JsonProperty("citation-title")
    public String getCitationTitle() {
        return citationTitle;
    }

    @JsonProperty("citation-title")
    public void setCitationTitle(String citationTitle) {
        this.citationTitle = citationTitle;
    }

    @JsonProperty("author-group")
    public List<AuthorGroup> getAuthorGroup() {
        return authorGroup;
    }

    @JsonProperty("author-group")
    public void setAuthorGroup(List<AuthorGroup> authorGroup) {
        this.authorGroup = authorGroup;
    }

    @JsonProperty("correspondence")
    public Correspondence getCorrespondence() {
        return correspondence;
    }

    @JsonProperty("correspondence")
    public void setCorrespondence(Correspondence correspondence) {
        this.correspondence = correspondence;
    }

    @JsonProperty("abstracts")
    public String getAbstracts() {
        return abstracts;
    }

    @JsonProperty("abstracts")
    public void setAbstracts(String abstracts) {
        this.abstracts = abstracts;
    }

    @JsonProperty("source")
    public Source getSource() {
        return source;
    }

    @JsonProperty("source")
    public void setSource(Source source) {
        this.source = source;
    }

    @JsonProperty("enhancement")
    public Enhancement getEnhancement() {
        return enhancement;
    }

    @JsonProperty("enhancement")
    public void setEnhancement(Enhancement enhancement) {
        this.enhancement = enhancement;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
