
package singleWorkDetails;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "ait:process-info",
    "bibrecord"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class Item {

    @JsonProperty("ait:process-info")
    private AitProcessInfo aitProcessInfo;
    @JsonProperty("bibrecord")
    private Bibrecord bibrecord;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("ait:process-info")
    public AitProcessInfo getAitProcessInfo() {
        return aitProcessInfo;
    }

    @JsonProperty("ait:process-info")
    public void setAitProcessInfo(AitProcessInfo aitProcessInfo) {
        this.aitProcessInfo = aitProcessInfo;
    }

    @JsonProperty("bibrecord")
    public Bibrecord getBibrecord() {
        return bibrecord;
    }

    @JsonProperty("bibrecord")
    public void setBibrecord(Bibrecord bibrecord) {
        this.bibrecord = bibrecord;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
