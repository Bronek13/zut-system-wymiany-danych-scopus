
package singleWorkDetails;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "person",
    "affiliation",
    "ce:e-address"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class Correspondence {

    @JsonProperty("person")
    private Person person;
    @JsonProperty("affiliation")
    private Affiliation____ affiliation;
    @JsonProperty("ce:e-address")
    private CeEAddress_ ceEAddress;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("person")
    public Person getPerson() {
        return person;
    }

    @JsonProperty("person")
    public void setPerson(Person person) {
        this.person = person;
    }

    @JsonProperty("affiliation")
    public Affiliation____ getAffiliation() {
        return affiliation;
    }

    @JsonProperty("affiliation")
    public void setAffiliation(Affiliation____ affiliation) {
        this.affiliation = affiliation;
    }

    @JsonProperty("ce:e-address")
    public CeEAddress_ getCeEAddress() {
        return ceEAddress;
    }

    @JsonProperty("ce:e-address")
    public void setCeEAddress(CeEAddress_ ceEAddress) {
        this.ceEAddress = ceEAddress;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
