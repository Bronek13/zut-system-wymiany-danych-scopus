
package singleWorkDetails;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "@_fa",
    "@auid",
    "@seq",
    "ce:initials",
    "ce:indexed-name",
    "ce:surname",
    "preferred-name",
    "author-url",
    "affiliation"
})
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
public class Author_ {

    @JsonProperty("@_fa")
    private String fa;
    @JsonProperty("@auid")
    private String auid;
    @JsonProperty("@seq")
    private String seq;
    @JsonProperty("ce:initials")
    private String ceInitials;
    @JsonProperty("ce:indexed-name")
    private String ceIndexedName;
    @JsonProperty("ce:surname")
    private String ceSurname;
    @JsonProperty("preferred-name")
    private PreferredName_ preferredName;
    @JsonProperty("author-url")
    private String authorUrl;
    @JsonProperty("affiliation")
    @JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
    private Affiliation__[] affiliation;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("@_fa")
    public String getFa() {
        return fa;
    }

    @JsonProperty("@_fa")
    public void setFa(String fa) {
        this.fa = fa;
    }

    @JsonProperty("@auid")
    public String getAuid() {
        return auid;
    }

    @JsonProperty("@auid")
    public void setAuid(String auid) {
        this.auid = auid;
    }

    @JsonProperty("@seq")
    public String getSeq() {
        return seq;
    }

    @JsonProperty("@seq")
    public void setSeq(String seq) {
        this.seq = seq;
    }

    @JsonProperty("ce:initials")
    public String getCeInitials() {
        return ceInitials;
    }

    @JsonProperty("ce:initials")
    public void setCeInitials(String ceInitials) {
        this.ceInitials = ceInitials;
    }

    @JsonProperty("ce:indexed-name")
    public String getCeIndexedName() {
        return ceIndexedName;
    }

    @JsonProperty("ce:indexed-name")
    public void setCeIndexedName(String ceIndexedName) {
        this.ceIndexedName = ceIndexedName;
    }

    @JsonProperty("ce:surname")
    public String getCeSurname() {
        return ceSurname;
    }

    @JsonProperty("ce:surname")
    public void setCeSurname(String ceSurname) {
        this.ceSurname = ceSurname;
    }

    @JsonProperty("preferred-name")
    public PreferredName_ getPreferredName() {
        return preferredName;
    }

    @JsonProperty("preferred-name")
    public void setPreferredName(PreferredName_ preferredName) {
        this.preferredName = preferredName;
    }

    @JsonProperty("author-url")
    public String getAuthorUrl() {
        return authorUrl;
    }

    @JsonProperty("author-url")
    public void setAuthorUrl(String authorUrl) {
        this.authorUrl = authorUrl;
    }

    @JsonProperty("affiliation")
    public Affiliation__[] getAffiliation() {
        return affiliation;
    }

    @JsonProperty("affiliation")
    public void setAffiliation(Affiliation__[] affiliation) {
        this.affiliation = affiliation;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
