
package singleWorkDetails;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "@href",
    "@id",
    "affilname"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class Affiliation_ {

    @JsonProperty("@href")
    private String href;
    @JsonProperty("@id")
    private String id;
    @JsonProperty("affilname")
    private String affilname;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("@href")
    public String getHref() {
        return href;
    }

    @JsonProperty("@href")
    public void setHref(String href) {
        this.href = href;
    }

    @JsonProperty("@id")
    public String getId() {
        return id;
    }

    @JsonProperty("@id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("affilname")
    public String getAffilname() {
        return affilname;
    }

    @JsonProperty("affilname")
    public void setAffilname(String affilname) {
        this.affilname = affilname;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
