
package singleWorkDetails;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "descriptors"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class Descriptorgroup {

    @JsonProperty("descriptors")
    private List<Descriptor> descriptors = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("descriptors")
    public List<Descriptor> getDescriptors() {
        return descriptors;
    }

    @JsonProperty("descriptors")
    public void setDescriptors(List<Descriptor> descriptors) {
        this.descriptors = descriptors;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
