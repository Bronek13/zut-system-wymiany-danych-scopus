
package singleWorkDetails;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "voliss",
    "pagerange"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class Volisspag {

    @JsonProperty("voliss")
    private Voliss voliss;
    @JsonProperty("pagerange")
    private Pagerange pagerange;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("voliss")
    public Voliss getVoliss() {
        return voliss;
    }

    @JsonProperty("voliss")
    public void setVoliss(Voliss voliss) {
        this.voliss = voliss;
    }

    @JsonProperty("pagerange")
    public Pagerange getPagerange() {
        return pagerange;
    }

    @JsonProperty("pagerange")
    public void setPagerange(Pagerange pagerange) {
        this.pagerange = pagerange;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
