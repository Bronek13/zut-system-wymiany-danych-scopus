
package singleWorkDetails;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "author-keyword"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class AuthorKeywords {

    @JsonProperty("author-keyword")
    private List<AuthorKeyword_> authorKeyword = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("author-keyword")
    public List<AuthorKeyword_> getAuthorKeyword() {
        return authorKeyword;
    }

    @JsonProperty("author-keyword")
    public void setAuthorKeyword(List<AuthorKeyword_> authorKeyword) {
        this.authorKeyword = authorKeyword;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
