
package singleWorkDetails;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "@controlled",
    "@type",
    "descriptor"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class Descriptor {

    @JsonProperty("@controlled")
    private String controlled;
    @JsonProperty("@type")
    private String type;
    @JsonProperty("descriptor")
    private List<Descriptor_> descriptor = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("@controlled")
    public String getControlled() {
        return controlled;
    }

    @JsonProperty("@controlled")
    public void setControlled(String controlled) {
        this.controlled = controlled;
    }

    @JsonProperty("@type")
    public String getType() {
        return type;
    }

    @JsonProperty("@type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("descriptor")
    public List<Descriptor_> getDescriptor() {
        return descriptor;
    }

    @JsonProperty("descriptor")
    public void setDescriptor(List<Descriptor_> descriptor) {
        this.descriptor = descriptor;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
