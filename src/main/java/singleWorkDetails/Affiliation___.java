
package singleWorkDetails;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "@afid",
    "@country",
    "@dptid",
    "organization",
    "address-part",
    "city-group"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class Affiliation___ {

    @JsonProperty("@afid")
    private String afid;
    @JsonProperty("@country")
    private String country;
    @JsonProperty("@dptid")
    private String dptid;
    @JsonProperty("organization")
    private List<Organization> organization = null;
    @JsonProperty("address-part")
    private String addressPart;
    @JsonProperty("city-group")
    private String cityGroup;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("@afid")
    public String getAfid() {
        return afid;
    }

    @JsonProperty("@afid")
    public void setAfid(String afid) {
        this.afid = afid;
    }

    @JsonProperty("@country")
    public String getCountry() {
        return country;
    }

    @JsonProperty("@country")
    public void setCountry(String country) {
        this.country = country;
    }

    @JsonProperty("@dptid")
    public String getDptid() {
        return dptid;
    }

    @JsonProperty("@dptid")
    public void setDptid(String dptid) {
        this.dptid = dptid;
    }

    @JsonProperty("organization")
    public List<Organization> getOrganization() {
        return organization;
    }

    @JsonProperty("organization")
    public void setOrganization(List<Organization> organization) {
        this.organization = organization;
    }

    @JsonProperty("address-part")
    public String getAddressPart() {
        return addressPart;
    }

    @JsonProperty("address-part")
    public void setAddressPart(String addressPart) {
        this.addressPart = addressPart;
    }

    @JsonProperty("city-group")
    public String getCityGroup() {
        return cityGroup;
    }

    @JsonProperty("city-group")
    public void setCityGroup(String cityGroup) {
        this.cityGroup = cityGroup;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
