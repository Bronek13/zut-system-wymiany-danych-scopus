
package singleWorkDetails;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "author",
    "affiliation"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class AuthorGroup {

    @JsonProperty("author")
    private Author__ author;
    @JsonProperty("affiliation")
    private Affiliation___ affiliation;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("author")
    public Author__ getAuthor() {
        return author;
    }

    @JsonProperty("author")
    public void setAuthor(Author__ author) {
        this.author = author;
    }

    @JsonProperty("affiliation")
    public Affiliation___ getAffiliation() {
        return affiliation;
    }

    @JsonProperty("affiliation")
    public void setAffiliation(Affiliation___ affiliation) {
        this.affiliation = affiliation;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
