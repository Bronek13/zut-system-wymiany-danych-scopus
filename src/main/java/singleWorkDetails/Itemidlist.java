
package singleWorkDetails;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "ce:pii",
    "ce:doi",
    "itemid"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class Itemidlist {

    @JsonProperty("ce:pii")
    private String cePii;
    @JsonProperty("ce:doi")
    private String ceDoi;
    @JsonProperty("itemid")
    private List<Itemid> itemid = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("ce:pii")
    public String getCePii() {
        return cePii;
    }

    @JsonProperty("ce:pii")
    public void setCePii(String cePii) {
        this.cePii = cePii;
    }

    @JsonProperty("ce:doi")
    public String getCeDoi() {
        return ceDoi;
    }

    @JsonProperty("ce:doi")
    public void setCeDoi(String ceDoi) {
        this.ceDoi = ceDoi;
    }

    @JsonProperty("itemid")
    public List<Itemid> getItemid() {
        return itemid;
    }

    @JsonProperty("itemid")
    public void setItemid(List<Itemid> itemid) {
        this.itemid = itemid;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
