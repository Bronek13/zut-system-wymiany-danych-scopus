
package singleWorkDetails;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "chemicals"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class Chemicalgroup {

    @JsonProperty("chemicals")
    private Chemicals chemicals;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("chemicals")
    public Chemicals getChemicals() {
        return chemicals;
    }

    @JsonProperty("chemicals")
    public void setChemicals(Chemicals chemicals) {
        this.chemicals = chemicals;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
