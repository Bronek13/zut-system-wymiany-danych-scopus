
package singleWorkDetails;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "@type",
    "classification"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class Classification {

    @JsonProperty("@type")
    private String type;
    @JsonProperty("classification")
    private List<Classification_> classification = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("@type")
    public String getType() {
        return type;
    }

    @JsonProperty("@type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("classification")
    public List<Classification_> getClassification() {
        return classification;
    }

    @JsonProperty("classification")
    public void setClassification(List<Classification_> classification) {
        this.classification = classification;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
