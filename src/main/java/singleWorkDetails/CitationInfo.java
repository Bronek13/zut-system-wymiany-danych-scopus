
package singleWorkDetails;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "citation-type",
    "citation-language",
    "abstract-language",
    "author-keywords"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class CitationInfo {

    @JsonProperty("citation-type")
    private CitationType citationType;
    @JsonProperty("citation-language")
    private CitationLanguage citationLanguage;
    @JsonProperty("abstract-language")
    private AbstractLanguage abstractLanguage;
    @JsonProperty("author-keywords")
    private AuthorKeywords authorKeywords;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("citation-type")
    public CitationType getCitationType() {
        return citationType;
    }

    @JsonProperty("citation-type")
    public void setCitationType(CitationType citationType) {
        this.citationType = citationType;
    }

    @JsonProperty("citation-language")
    public CitationLanguage getCitationLanguage() {
        return citationLanguage;
    }

    @JsonProperty("citation-language")
    public void setCitationLanguage(CitationLanguage citationLanguage) {
        this.citationLanguage = citationLanguage;
    }

    @JsonProperty("abstract-language")
    public AbstractLanguage getAbstractLanguage() {
        return abstractLanguage;
    }

    @JsonProperty("abstract-language")
    public void setAbstractLanguage(AbstractLanguage abstractLanguage) {
        this.abstractLanguage = abstractLanguage;
    }

    @JsonProperty("author-keywords")
    public AuthorKeywords getAuthorKeywords() {
        return authorKeywords;
    }

    @JsonProperty("author-keywords")
    public void setAuthorKeywords(AuthorKeywords authorKeywords) {
        this.authorKeywords = authorKeywords;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
