
package singleWorkDetails;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "refd-itemidlist",
    "ref-authors",
    "ref-sourcetitle",
    "ref-publicationyear",
    "ref-volisspag",
    "ref-text"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class RefInfo {

    @JsonProperty("refd-itemidlist")
    private RefdItemidlist refdItemidlist;
    @JsonProperty("ref-authors")
    private RefAuthors refAuthors;
    @JsonProperty("ref-sourcetitle")
    private String refSourcetitle;
    @JsonProperty("ref-publicationyear")
    private RefPublicationyear refPublicationyear;
    @JsonProperty("ref-volisspag")
    private RefVolisspag refVolisspag;
    @JsonProperty("ref-text")
    private String refText;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("refd-itemidlist")
    public RefdItemidlist getRefdItemidlist() {
        return refdItemidlist;
    }

    @JsonProperty("refd-itemidlist")
    public void setRefdItemidlist(RefdItemidlist refdItemidlist) {
        this.refdItemidlist = refdItemidlist;
    }

    @JsonProperty("ref-authors")
    public RefAuthors getRefAuthors() {
        return refAuthors;
    }

    @JsonProperty("ref-authors")
    public void setRefAuthors(RefAuthors refAuthors) {
        this.refAuthors = refAuthors;
    }

    @JsonProperty("ref-sourcetitle")
    public String getRefSourcetitle() {
        return refSourcetitle;
    }

    @JsonProperty("ref-sourcetitle")
    public void setRefSourcetitle(String refSourcetitle) {
        this.refSourcetitle = refSourcetitle;
    }

    @JsonProperty("ref-publicationyear")
    public RefPublicationyear getRefPublicationyear() {
        return refPublicationyear;
    }

    @JsonProperty("ref-publicationyear")
    public void setRefPublicationyear(RefPublicationyear refPublicationyear) {
        this.refPublicationyear = refPublicationyear;
    }

    @JsonProperty("ref-volisspag")
    public RefVolisspag getRefVolisspag() {
        return refVolisspag;
    }

    @JsonProperty("ref-volisspag")
    public void setRefVolisspag(RefVolisspag refVolisspag) {
        this.refVolisspag = refVolisspag;
    }

    @JsonProperty("ref-text")
    public String getRefText() {
        return refText;
    }

    @JsonProperty("ref-text")
    public void setRefText(String refText) {
        this.refText = refText;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
