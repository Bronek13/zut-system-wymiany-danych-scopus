
package singleWorkDetails;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "mainterm"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class Idxterms {

    @JsonProperty("mainterm")
    private List<Mainterm> mainterm = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("mainterm")
    public List<Mainterm> getMainterm() {
        return mainterm;
    }

    @JsonProperty("mainterm")
    public void setMainterm(List<Mainterm> mainterm) {
        this.mainterm = mainterm;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
