
package singleWorkDetails;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "@day",
    "@month",
    "@year"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class AitDateSort {

    @JsonProperty("@day")
    private String day;
    @JsonProperty("@month")
    private String month;
    @JsonProperty("@year")
    private String year;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("@day")
    public String getDay() {
        return day;
    }

    @JsonProperty("@day")
    public void setDay(String day) {
        this.day = day;
    }

    @JsonProperty("@month")
    public String getMonth() {
        return month;
    }

    @JsonProperty("@month")
    public void setMonth(String month) {
        this.month = month;
    }

    @JsonProperty("@year")
    public String getYear() {
        return year;
    }

    @JsonProperty("@year")
    public void setYear(String year) {
        this.year = year;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
