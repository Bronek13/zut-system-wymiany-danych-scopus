
package singleWorkDetails;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "@day",
    "@month",
    "@timestamp",
    "@year"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class AitDateDelivered {

    @JsonProperty("@day")
    private String day;
    @JsonProperty("@month")
    private String month;
    @JsonProperty("@timestamp")
    private String timestamp;
    @JsonProperty("@year")
    private String year;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("@day")
    public String getDay() {
        return day;
    }

    @JsonProperty("@day")
    public void setDay(String day) {
        this.day = day;
    }

    @JsonProperty("@month")
    public String getMonth() {
        return month;
    }

    @JsonProperty("@month")
    public void setMonth(String month) {
        this.month = month;
    }

    @JsonProperty("@timestamp")
    public String getTimestamp() {
        return timestamp;
    }

    @JsonProperty("@timestamp")
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    @JsonProperty("@year")
    public String getYear() {
        return year;
    }

    @JsonProperty("@year")
    public void setYear(String year) {
        this.year = year;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
