
package singleWorkDetails;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "@refcount",
    "reference"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class Bibliography {

    @JsonProperty("@refcount")
    private String refcount;
    @JsonProperty("reference")
    private List<Reference> reference = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("@refcount")
    public String getRefcount() {
        return refcount;
    }

    @JsonProperty("@refcount")
    public void setRefcount(String refcount) {
        this.refcount = refcount;
    }

    @JsonProperty("reference")
    public List<Reference> getReference() {
        return reference;
    }

    @JsonProperty("reference")
    public void setReference(List<Reference> reference) {
        this.reference = reference;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
