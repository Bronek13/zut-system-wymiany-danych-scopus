
package singleWorkDetails;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "@issue",
    "@volume"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class Voliss {

    @JsonProperty("@issue")
    private String issue;
    @JsonProperty("@volume")
    private String volume;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("@issue")
    public String getIssue() {
        return issue;
    }

    @JsonProperty("@issue")
    public void setIssue(String issue) {
        this.issue = issue;
    }

    @JsonProperty("@volume")
    public String getVolume() {
        return volume;
    }

    @JsonProperty("@volume")
    public void setVolume(String volume) {
        this.volume = volume;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
