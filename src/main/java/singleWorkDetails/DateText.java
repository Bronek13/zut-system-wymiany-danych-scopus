
package singleWorkDetails;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "@xfab-added",
    "$"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class DateText {

    @JsonProperty("@xfab-added")
    private String xfabAdded;
    @JsonProperty("$")
    private String $;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("@xfab-added")
    public String getXfabAdded() {
        return xfabAdded;
    }

    @JsonProperty("@xfab-added")
    public void setXfabAdded(String xfabAdded) {
        this.xfabAdded = xfabAdded;
    }

    @JsonProperty("$")
    public String get$() {
        return $;
    }

    @JsonProperty("$")
    public void set$(String $) {
        this.$ = $;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
