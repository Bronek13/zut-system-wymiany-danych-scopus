
package singleWorkDetails;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "mainterm",
    "link"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class Descriptor_ {

    @JsonProperty("mainterm")
    private Mainterm_ mainterm;
    @JsonProperty("link")
    private String link;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("mainterm")
    public Mainterm_ getMainterm() {
        return mainterm;
    }

    @JsonProperty("mainterm")
    public void setMainterm(Mainterm_ mainterm) {
        this.mainterm = mainterm;
    }

    @JsonProperty("link")
    public String getLink() {
        return link;
    }

    @JsonProperty("link")
    public void setLink(String link) {
        this.link = link;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
