
package singleWorkDetails;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "ait:date-delivered",
    "ait:date-sort",
    "ait:status"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class AitProcessInfo {

    @JsonProperty("ait:date-delivered")
    private AitDateDelivered aitDateDelivered;
    @JsonProperty("ait:date-sort")
    private AitDateSort aitDateSort;
    @JsonProperty("ait:status")
    private AitStatus aitStatus;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("ait:date-delivered")
    public AitDateDelivered getAitDateDelivered() {
        return aitDateDelivered;
    }

    @JsonProperty("ait:date-delivered")
    public void setAitDateDelivered(AitDateDelivered aitDateDelivered) {
        this.aitDateDelivered = aitDateDelivered;
    }

    @JsonProperty("ait:date-sort")
    public AitDateSort getAitDateSort() {
        return aitDateSort;
    }

    @JsonProperty("ait:date-sort")
    public void setAitDateSort(AitDateSort aitDateSort) {
        this.aitDateSort = aitDateSort;
    }

    @JsonProperty("ait:status")
    public AitStatus getAitStatus() {
        return aitStatus;
    }

    @JsonProperty("ait:status")
    public void setAitStatus(AitStatus aitStatus) {
        this.aitStatus = aitStatus;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
