
package singleWorkDetails;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "ce:initials",
    "ce:indexed-name",
    "ce:surname",
    "ce:given-name"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class PreferredName {

    @JsonProperty("ce:initials")
    private String ceInitials;
    @JsonProperty("ce:indexed-name")
    private String ceIndexedName;
    @JsonProperty("ce:surname")
    private String ceSurname;
    @JsonProperty("ce:given-name")
    private String ceGivenName;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("ce:initials")
    public String getCeInitials() {
        return ceInitials;
    }

    @JsonProperty("ce:initials")
    public void setCeInitials(String ceInitials) {
        this.ceInitials = ceInitials;
    }

    @JsonProperty("ce:indexed-name")
    public String getCeIndexedName() {
        return ceIndexedName;
    }

    @JsonProperty("ce:indexed-name")
    public void setCeIndexedName(String ceIndexedName) {
        this.ceIndexedName = ceIndexedName;
    }

    @JsonProperty("ce:surname")
    public String getCeSurname() {
        return ceSurname;
    }

    @JsonProperty("ce:surname")
    public void setCeSurname(String ceSurname) {
        this.ceSurname = ceSurname;
    }

    @JsonProperty("ce:given-name")
    public String getCeGivenName() {
        return ceGivenName;
    }

    @JsonProperty("ce:given-name")
    public void setCeGivenName(String ceGivenName) {
        this.ceGivenName = ceGivenName;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
