
package singleWorkDetails;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "@country",
    "@srcid",
    "@type",
    "sourcetitle",
    "sourcetitle-abbrev",
    "issn",
    "codencode",
    "volisspag",
    "publicationyear",
    "publicationdate"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class Source {

    @JsonProperty("@country")
    private String country;
    @JsonProperty("@srcid")
    private String srcid;
    @JsonProperty("@type")
    private String type;
    @JsonProperty("sourcetitle")
    private String sourcetitle;
    @JsonProperty("sourcetitle-abbrev")
    private String sourcetitleAbbrev;
    @JsonProperty("issn")
    private Issn issn;
    @JsonProperty("codencode")
    private String codencode;
    @JsonProperty("volisspag")
    private Volisspag volisspag;
    @JsonProperty("publicationyear")
    private Publicationyear publicationyear;
    @JsonProperty("publicationdate")
    private Publicationdate publicationdate;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("@country")
    public String getCountry() {
        return country;
    }

    @JsonProperty("@country")
    public void setCountry(String country) {
        this.country = country;
    }

    @JsonProperty("@srcid")
    public String getSrcid() {
        return srcid;
    }

    @JsonProperty("@srcid")
    public void setSrcid(String srcid) {
        this.srcid = srcid;
    }

    @JsonProperty("@type")
    public String getType() {
        return type;
    }

    @JsonProperty("@type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("sourcetitle")
    public String getSourcetitle() {
        return sourcetitle;
    }

    @JsonProperty("sourcetitle")
    public void setSourcetitle(String sourcetitle) {
        this.sourcetitle = sourcetitle;
    }

    @JsonProperty("sourcetitle-abbrev")
    public String getSourcetitleAbbrev() {
        return sourcetitleAbbrev;
    }

    @JsonProperty("sourcetitle-abbrev")
    public void setSourcetitleAbbrev(String sourcetitleAbbrev) {
        this.sourcetitleAbbrev = sourcetitleAbbrev;
    }

    @JsonProperty("issn")
    public Issn getIssn() {
        return issn;
    }

    @JsonProperty("issn")
    public void setIssn(Issn issn) {
        this.issn = issn;
    }

    @JsonProperty("codencode")
    public String getCodencode() {
        return codencode;
    }

    @JsonProperty("codencode")
    public void setCodencode(String codencode) {
        this.codencode = codencode;
    }

    @JsonProperty("volisspag")
    public Volisspag getVolisspag() {
        return volisspag;
    }

    @JsonProperty("volisspag")
    public void setVolisspag(Volisspag volisspag) {
        this.volisspag = volisspag;
    }

    @JsonProperty("publicationyear")
    public Publicationyear getPublicationyear() {
        return publicationyear;
    }

    @JsonProperty("publicationyear")
    public void setPublicationyear(Publicationyear publicationyear) {
        this.publicationyear = publicationyear;
    }

    @JsonProperty("publicationdate")
    public Publicationdate getPublicationdate() {
        return publicationdate;
    }

    @JsonProperty("publicationdate")
    public void setPublicationdate(Publicationdate publicationdate) {
        this.publicationdate = publicationdate;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
