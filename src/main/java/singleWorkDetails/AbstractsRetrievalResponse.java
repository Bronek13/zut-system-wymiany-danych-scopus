
package singleWorkDetails;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "coredata",
    "affiliation",
    "authors",
    "language",
    "authkeywords",
    "idxterms",
    "subject-areas",
    "item"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class AbstractsRetrievalResponse {

    @JsonProperty("coredata")
    private Coredata coredata;
    @JsonProperty("affiliation")
    private List<Affiliation_> affiliation = null;
    @JsonProperty("authors")
    private Authors authors;
    @JsonProperty("language")
    private Language language;
    @JsonProperty("authkeywords")
    private Authkeywords authkeywords;
    @JsonProperty("idxterms")
    private Idxterms idxterms;
    @JsonProperty("subject-areas")
    private SubjectAreas subjectAreas;
    @JsonProperty("item")
    private Item item;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("coredata")
    public Coredata getCoredata() {
        return coredata;
    }

    @JsonProperty("coredata")
    public void setCoredata(Coredata coredata) {
        this.coredata = coredata;
    }

    @JsonProperty("affiliation")
    public List<Affiliation_> getAffiliation() {
        return affiliation;
    }

    @JsonProperty("affiliation")
    public void setAffiliation(List<Affiliation_> affiliation) {
        this.affiliation = affiliation;
    }

    @JsonProperty("authors")
    public Authors getAuthors() {
        return authors;
    }

    @JsonProperty("authors")
    public void setAuthors(Authors authors) {
        this.authors = authors;
    }

    @JsonProperty("language")
    public Language getLanguage() {
        return language;
    }

    @JsonProperty("language")
    public void setLanguage(Language language) {
        this.language = language;
    }

    @JsonProperty("authkeywords")
    public Authkeywords getAuthkeywords() {
        return authkeywords;
    }

    @JsonProperty("authkeywords")
    public void setAuthkeywords(Authkeywords authkeywords) {
        this.authkeywords = authkeywords;
    }

    @JsonProperty("idxterms")
    public Idxterms getIdxterms() {
        return idxterms;
    }

    @JsonProperty("idxterms")
    public void setIdxterms(Idxterms idxterms) {
        this.idxterms = idxterms;
    }

    @JsonProperty("subject-areas")
    public SubjectAreas getSubjectAreas() {
        return subjectAreas;
    }

    @JsonProperty("subject-areas")
    public void setSubjectAreas(SubjectAreas subjectAreas) {
        this.subjectAreas = subjectAreas;
    }

    @JsonProperty("item")
    public Item getItem() {
        return item;
    }

    @JsonProperty("item")
    public void setItem(Item item) {
        this.item = item;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
