
package singleWorkDetails;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "@stage",
    "@state",
    "@type"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class AitStatus {

    @JsonProperty("@stage")
    private String stage;
    @JsonProperty("@state")
    private String state;
    @JsonProperty("@type")
    private String type;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("@stage")
    public String getStage() {
        return stage;
    }

    @JsonProperty("@stage")
    public void setStage(String stage) {
        this.stage = stage;
    }

    @JsonProperty("@state")
    public String getState() {
        return state;
    }

    @JsonProperty("@state")
    public void setState(String state) {
        this.state = state;
    }

    @JsonProperty("@type")
    public String getType() {
        return type;
    }

    @JsonProperty("@type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
