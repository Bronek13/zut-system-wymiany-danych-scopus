
package singleWorkDetails;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "bibliography"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class Tail {

    @JsonProperty("bibliography")
    private Bibliography bibliography;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("bibliography")
    public Bibliography getBibliography() {
        return bibliography;
    }

    @JsonProperty("bibliography")
    public void setBibliography(Bibliography bibliography) {
        this.bibliography = bibliography;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
