
package singleWorkDetails;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "@first"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class RefPublicationyear {

    @JsonProperty("@first")
    private String first;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("@first")
    public String getFirst() {
        return first;
    }

    @JsonProperty("@first")
    public void setFirst(String first) {
        this.first = first;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
