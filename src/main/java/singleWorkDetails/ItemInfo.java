
package singleWorkDetails;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "copyright",
    "itemidlist",
    "history",
    "dbcollection"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class ItemInfo {

    @JsonProperty("copyright")
    private List<Copyright> copyright = null;
    @JsonProperty("itemidlist")
    private Itemidlist itemidlist;
    @JsonProperty("history")
    private History history;
    @JsonProperty("dbcollection")
    private List<Dbcollection> dbcollection = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("copyright")
    public List<Copyright> getCopyright() {
        return copyright;
    }

    @JsonProperty("copyright")
    public void setCopyright(List<Copyright> copyright) {
        this.copyright = copyright;
    }

    @JsonProperty("itemidlist")
    public Itemidlist getItemidlist() {
        return itemidlist;
    }

    @JsonProperty("itemidlist")
    public void setItemidlist(Itemidlist itemidlist) {
        this.itemidlist = itemidlist;
    }

    @JsonProperty("history")
    public History getHistory() {
        return history;
    }

    @JsonProperty("history")
    public void setHistory(History history) {
        this.history = history;
    }

    @JsonProperty("dbcollection")
    public List<Dbcollection> getDbcollection() {
        return dbcollection;
    }

    @JsonProperty("dbcollection")
    public void setDbcollection(List<Dbcollection> dbcollection) {
        this.dbcollection = dbcollection;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
