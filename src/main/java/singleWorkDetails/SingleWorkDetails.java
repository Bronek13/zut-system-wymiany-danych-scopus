
package singleWorkDetails;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "abstracts-retrieval-response"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class SingleWorkDetails {

    @JsonProperty("abstracts-retrieval-response")
    private AbstractsRetrievalResponse abstractsRetrievalResponse;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("abstracts-retrieval-response")
    public AbstractsRetrievalResponse getAbstractsRetrievalResponse() {
        return abstractsRetrievalResponse;
    }

    @JsonProperty("abstracts-retrieval-response")
    public void setAbstractsRetrievalResponse(AbstractsRetrievalResponse abstractsRetrievalResponse) {
        this.abstractsRetrievalResponse = abstractsRetrievalResponse;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
