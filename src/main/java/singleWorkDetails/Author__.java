
package singleWorkDetails;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "@auid",
    "@seq",
    "ce:initials",
    "ce:indexed-name",
    "ce:surname",
    "preferred-name",
    "ce:e-address"
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class Author__ {

    @JsonProperty("@auid")
    private String auid;
    @JsonProperty("@seq")
    private String seq;
    @JsonProperty("ce:initials")
    private String ceInitials;
    @JsonProperty("ce:indexed-name")
    private String ceIndexedName;
    @JsonProperty("ce:surname")
    private String ceSurname;
    @JsonProperty("preferred-name")
    private PreferredName__ preferredName;
    @JsonProperty("ce:e-address")
    private CeEAddress ceEAddress;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("@auid")
    public String getAuid() {
        return auid;
    }

    @JsonProperty("@auid")
    public void setAuid(String auid) {
        this.auid = auid;
    }

    @JsonProperty("@seq")
    public String getSeq() {
        return seq;
    }

    @JsonProperty("@seq")
    public void setSeq(String seq) {
        this.seq = seq;
    }

    @JsonProperty("ce:initials")
    public String getCeInitials() {
        return ceInitials;
    }

    @JsonProperty("ce:initials")
    public void setCeInitials(String ceInitials) {
        this.ceInitials = ceInitials;
    }

    @JsonProperty("ce:indexed-name")
    public String getCeIndexedName() {
        return ceIndexedName;
    }

    @JsonProperty("ce:indexed-name")
    public void setCeIndexedName(String ceIndexedName) {
        this.ceIndexedName = ceIndexedName;
    }

    @JsonProperty("ce:surname")
    public String getCeSurname() {
        return ceSurname;
    }

    @JsonProperty("ce:surname")
    public void setCeSurname(String ceSurname) {
        this.ceSurname = ceSurname;
    }

    @JsonProperty("preferred-name")
    public PreferredName__ getPreferredName() {
        return preferredName;
    }

    @JsonProperty("preferred-name")
    public void setPreferredName(PreferredName__ preferredName) {
        this.preferredName = preferredName;
    }

    @JsonProperty("ce:e-address")
    public CeEAddress getCeEAddress() {
        return ceEAddress;
    }

    @JsonProperty("ce:e-address")
    public void setCeEAddress(CeEAddress ceEAddress) {
        this.ceEAddress = ceEAddress;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
