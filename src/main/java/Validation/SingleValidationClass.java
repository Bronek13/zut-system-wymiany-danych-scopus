
package Validation;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "titles",
    "doi"
})
public class SingleValidationClass {

    @JsonProperty("titles")
    private Titles titles;
    @JsonProperty("doi")
    private Doi doi;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("titles")
    public Titles getTitles() {
        return titles;
    }

    @JsonProperty("titles")
    public void setTitles(Titles titles) {
        this.titles = titles;
    }

    @JsonProperty("doi")
    public Doi getDoi() {
        return doi;
    }

    @JsonProperty("doi")
    public void setDoi(Doi doi) {
        this.doi = doi;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
