
package Validation;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "singleValidationClass"
})
public class ValidationClass {

    @JsonProperty("singleValidationClass")
    private List<SingleValidationClass> singleValidationClass = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("singleValidationClass")
    public List<SingleValidationClass> getSingleValidationClass() {
        return singleValidationClass;
    }

    @JsonProperty("singleValidationClass")
    public void setSingleValidationClass(List<SingleValidationClass> singleValidationClass) {
        this.singleValidationClass = singleValidationClass;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
