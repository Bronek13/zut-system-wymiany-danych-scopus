
package Validation;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "orcidTitle",
    "scopusTitle",
    "validationResultTitle"
})
public class Titles {

    @JsonProperty("orcidTitle")
    private String orcidTitle;
    @JsonProperty("scopusTitle")
    private String scopusTitle;
    @JsonProperty("validationResultTitle")
    private String validationResultTitle;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("orcidTitle")
    public String getOrcidTitle() {
        return orcidTitle;
    }

    @JsonProperty("orcidTitle")
    public void setOrcidTitle(String orcidTitle) {
        this.orcidTitle = orcidTitle;
    }

    @JsonProperty("scopusTitle")
    public String getScopusTitle() {
        return scopusTitle;
    }

    @JsonProperty("scopusTitle")
    public void setScopusTitle(String scopusTitle) {
        this.scopusTitle = scopusTitle;
    }

    @JsonProperty("validationResultTitle")
    public String getValidationResultTitle() {
        return validationResultTitle;
    }

    @JsonProperty("validationResultTitle")
    public void setValidationResultTitle(String validationResultTitle) {
        this.validationResultTitle = validationResultTitle;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
