
package Validation;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "orcidDOI",
    "scopusDOI",
    "validationResultDOI"
})
public class Doi {

    @JsonProperty("orcidDOI")
    private String orcidDOI;
    @JsonProperty("scopusDOI")
    private String scopusDOI;
    @JsonProperty("validationResultDOI")
    private String validationResultDOI;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("orcidDOI")
    public String getOrcidDOI() {
        return orcidDOI;
    }

    @JsonProperty("orcidDOI")
    public void setOrcidDOI(String orcidDOI) {
        this.orcidDOI = orcidDOI;
    }

    @JsonProperty("scopusDOI")
    public String getScopusDOI() {
        return scopusDOI;
    }

    @JsonProperty("scopusDOI")
    public void setScopusDOI(String scopusDOI) {
        this.scopusDOI = scopusDOI;
    }

    @JsonProperty("validationResultDOI")
    public String getValidationResultDOI() {
        return validationResultDOI;
    }

    @JsonProperty("validationResultDOI")
    public void setValidationResultDOI(String validationResultDOI) {
        this.validationResultDOI = validationResultDOI;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
