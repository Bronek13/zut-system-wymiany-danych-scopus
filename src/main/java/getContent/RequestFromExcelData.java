package getContent;

public class RequestFromExcelData {

    private String name;
    private String surname;
    private String affiliation;
    private String dateStart;
    private String dateEnd;

    public String getDateStart() {
        return dateStart;
    }

    public void setDateStart(String dateStart) {
        this.dateStart = dateStart;
    }

    public String getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getAffiliation() {
        return affiliation;
    }

    public void setAffiliation(String affillation) {
        this.affiliation = affillation;
    }
}
