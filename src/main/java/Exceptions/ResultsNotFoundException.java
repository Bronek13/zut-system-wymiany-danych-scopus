package Exceptions;

public class ResultsNotFoundException extends Exception {

    public ResultsNotFoundException(String errorMessage) {
        super(errorMessage);
    }
}
