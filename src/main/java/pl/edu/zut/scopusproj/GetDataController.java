package pl.edu.zut.scopusproj;

import ResponseORCID.Work.GetWork;
import getContent.RequestData;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import java.text.ParseException;
import java.util.Date;

@RestController
public class GetDataController {

    private String tempIP = "http://192.168.0.171:8089";
    private RestTemplate restTemplate = new RestTemplate();

    public GetWork[] getData(String ordicId, Date dateStart, Date dateEnd) throws ParseException {

        RequestData requestData = new RequestData();
        requestData.setOrcid(ordicId);
        requestData.setDateStart(dateStart);
        requestData.setDateEnd(dateEnd);
        ResponseEntity<GetWork[]> responseEntity = restTemplate
                .postForEntity(tempIP + "/findworks", requestData, GetWork[].class);

        return responseEntity.getBody();
    }
}
