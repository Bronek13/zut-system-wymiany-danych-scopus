package pl.edu.zut.scopusproj;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.client.RestTemplate;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;


@Controller
public class GetContentController
{

	private RestTemplate restTemplate;
	private String apiKey="824dabe59c17805f8ff50ee0e78fd10d";
	
	public String getApiKey() {
		return apiKey;
	}

	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}

//	@GetMapping("/getDocs")
//	public ResponseEntity<GetContentDTO> getDocs() {
//
//		HttpHeaders headers = new HttpHeaders();
//		headers.setContentType(MediaType.APPLICATION_JSON);
//		headers.add("X-ELS-APIKey", apiKey);
//
//
//	ResponseEntity<GetContentDTO> docsEntity = restTemplate.getForEntity("http://api.elsevier.com/content/abstract/scopus_id/84943242635?field=authors,title,publicationName,volume,issueIdentifier,prism:pageRange,coverDate,article-number,doi,citedby-count,prism:aggregationType", GetContentDTO.class);
//
//	if (docsEntity.getStatusCode()==HttpStatus.OK) {
//
//		return new ResponseEntity<GetContentDTO>(getDocs().getBody(),
//				HttpStatus.OK);
//	} else {
//
//			return null;
//		}
//	}
}
