
package pl.edu.zut.scopusproj.worksData;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "@_fa",
    "prism:url",
    "dc:identifier"
})
public class Entry {

    @JsonProperty("@_fa")
    private String fa;
    @JsonProperty("prism:url")
    private String prismUrl;
    @JsonProperty("dc:identifier")
    private String dcIdentifier;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("@_fa")
    public String getFa() {
        return fa;
    }

    @JsonProperty("@_fa")
    public void setFa(String fa) {
        this.fa = fa;
    }

    @JsonProperty("prism:url")
    public String getPrismUrl() {
        return prismUrl;
    }

    @JsonProperty("prism:url")
    public void setPrismUrl(String prismUrl) {
        this.prismUrl = prismUrl;
    }

    @JsonProperty("dc:identifier")
    public String getDcIdentifier() {
        return dcIdentifier;
    }

    @JsonProperty("dc:identifier")
    public void setDcIdentifier(String dcIdentifier) {
        this.dcIdentifier = dcIdentifier;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
