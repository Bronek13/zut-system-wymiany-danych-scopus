
package pl.edu.zut.scopusproj.authorData;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AffiliationCurrent {

    @JsonProperty("affiliation-url")
    private String affiliationUrl;
    @JsonProperty("affiliation-id")
    private String affiliationId;
    @JsonProperty("affiliation-name")
    private String affiliationName;
    @JsonProperty("affiliation-city")
    private String affiliationCity;
    @JsonProperty("affiliation-country")
    private String affiliationCountry;

    @JsonProperty("affiliation-url")
    public String getAffiliationUrl() {
        return affiliationUrl;
    }

    @JsonProperty("affiliation-url")
    public void setAffiliationUrl(String affiliationUrl) {
        this.affiliationUrl = affiliationUrl;
    }

    @JsonProperty("affiliation-id")
    public String getAffiliationId() {
        return affiliationId;
    }

    @JsonProperty("affiliation-id")
    public void setAffiliationId(String affiliationId) {
        this.affiliationId = affiliationId;
    }

    @JsonProperty("affiliation-name")
    public String getAffiliationName() {
        return affiliationName;
    }

    @JsonProperty("affiliation-name")
    public void setAffiliationName(String affiliationName) {
        this.affiliationName = affiliationName;
    }

    @JsonProperty("affiliation-city")
    public String getAffiliationCity() {
        return affiliationCity;
    }

    @JsonProperty("affiliation-city")
    public void setAffiliationCity(String affiliationCity) {
        this.affiliationCity = affiliationCity;
    }

    @JsonProperty("affiliation-country")
    public String getAffiliationCountry() {
        return affiliationCountry;
    }

    @JsonProperty("affiliation-country")
    public void setAffiliationCountry(String affiliationCountry) {
        this.affiliationCountry = affiliationCountry;
    }

}
