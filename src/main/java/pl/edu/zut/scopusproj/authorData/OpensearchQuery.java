
package pl.edu.zut.scopusproj.authorData;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OpensearchQuery {

    @JsonProperty("@role")
    private String role;
    @JsonProperty("@searchTerms")
    private String searchTerms;
    @JsonProperty("@startPage")
    private String startPage;

    @JsonProperty("@role")
    public String getRole() {
        return role;
    }

    @JsonProperty("@role")
    public void setRole(String role) {
        this.role = role;
    }

    @JsonProperty("@searchTerms")
    public String getSearchTerms() {
        return searchTerms;
    }

    @JsonProperty("@searchTerms")
    public void setSearchTerms(String searchTerms) {
        this.searchTerms = searchTerms;
    }

    @JsonProperty("@startPage")
    public String getStartPage() {
        return startPage;
    }

    @JsonProperty("@startPage")
    public void setStartPage(String startPage) {
        this.startPage = startPage;
    }

}
