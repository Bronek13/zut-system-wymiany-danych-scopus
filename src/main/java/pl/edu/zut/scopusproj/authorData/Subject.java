
package pl.edu.zut.scopusproj.authorData;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Subject {

    @JsonProperty("@abbrev")
    private String abbr;
    @JsonProperty("@frequency")
    private String frequency;
    @JsonProperty("$")
    private String areas ;

    @JsonProperty("@abbrev")
    public String getAbbr() {
        return abbr;
    }

    @JsonProperty("@abbrev")
    public void setAbbr(String abbr) {
        this.abbr = abbr;
    }

    @JsonProperty("@frequency")
    public String getFrequency() {
        return frequency;
    }

    @JsonProperty("@frequency")
    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    @JsonProperty("$")
    public String getAreas() {
        return areas;
    }

    @JsonProperty("$")
    public void setAreas(String areas) {
        this.areas = areas;
    }

}
