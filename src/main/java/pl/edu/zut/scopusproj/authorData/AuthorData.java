
package pl.edu.zut.scopusproj.authorData;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AuthorData {

    @JsonProperty("search-results")
    private SearchResults searchResults;

    @JsonProperty("search-results")
    public SearchResults getSearchResults() {
        return searchResults;
    }

    @JsonProperty("search-results")
    public void setSearchResults(SearchResults searchResults) {
        this.searchResults = searchResults;
    }

}
