
package pl.edu.zut.scopusproj.authorData;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class NameVariant {

    @JsonProperty("@_fa")
    private String fa;
    @JsonProperty("surname")
    private String surname;
    @JsonProperty("given-name")
    private String givenName;
    @JsonProperty("initials")
    private String initials;

    @JsonProperty("@_fa")
    public String getFa() {
        return fa;
    }

    @JsonProperty("@_fa")
    public void setFa(String fa) {
        this.fa = fa;
    }

    @JsonProperty("surname")
    public String getSurname() {
        return surname;
    }

    @JsonProperty("surname")
    public void setSurname(String surname) {
        this.surname = surname;
    }

    @JsonProperty("given-name")
    public String getGivenName() {
        return givenName;
    }

    @JsonProperty("given-name")
    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    @JsonProperty("initials")
    public String getInitials() {
        return initials;
    }

    @JsonProperty("initials")
    public void setInitials(String initials) {
        this.initials = initials;
    }

}
