
package pl.edu.zut.scopusproj.authorData;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Entry {

    @JsonProperty("@_fa")
    private String fa;
    @JsonProperty("link")
    private List<Link_> link = null;
    @JsonProperty("prism:url")
    private String prismUrl;
    @JsonProperty("dc:identifier")
    private String dcIdentifier;
    @JsonProperty("eid")
    private String eid;
    @JsonProperty(value="orcid", required=false)
    private String orcid;
    @JsonProperty("preferred-name")
    private PreferredName preferredName;
    @JsonProperty(value="name-variant", required=false)
    private List<NameVariant> nameVariant = null;
    @JsonProperty("document-count")
    private String documentCount;
    @JsonProperty("subject-area")
    private List<Subject> subjectArea = null;
    @JsonProperty(value = "affiliation-current", required=false)
    private AffiliationCurrent affiliationCurrent;

    @JsonProperty("@_fa")
    public String getFa() {
        return fa;
    }

    @JsonProperty("@_fa")
    public void setFa(String fa) {
        this.fa = fa;
    }

    @JsonProperty("link")
    public List<Link_> getLink() {
        return link;
    }

    @JsonProperty("link")
    public void setLink(List<Link_> link) {
        this.link = link;
    }

    @JsonProperty("prism:url")
    public String getPrismUrl() {
        return prismUrl;
    }

    @JsonProperty("prism:url")
    public void setPrismUrl(String prismUrl) {
        this.prismUrl = prismUrl;
    }

    @JsonProperty("dc:identifier")
    public String getDcIdentifier() {
        return dcIdentifier;
    }

    @JsonProperty("dc:identifier")
    public void setDcIdentifier(String dcIdentifier) {
        this.dcIdentifier = dcIdentifier;
    }

    @JsonProperty("orcid")
    public String getOrcid() {
        return orcid;
    }

    @JsonProperty("orcid")
    public void setOrcid(String orcid) {
        this.orcid = orcid;
    }

    @JsonProperty("eid")
    public String getEid() {
        return eid;
    }

    @JsonProperty("eid")
    public void setEid(String eid) {
        this.eid = eid;
    }

    @JsonProperty("preferred-name")
    public PreferredName getPreferredName() {
        return preferredName;
    }

    @JsonProperty("preferred-name")
    public void setPreferredName(PreferredName preferredName) {
        this.preferredName = preferredName;
    }

    @JsonProperty("name-variant")
    public List<NameVariant> getNameVariant() {
        return nameVariant;
    }

    @JsonProperty("name-variant")
    public void setNameVariant(List<NameVariant> nameVariant) {
        this.nameVariant = nameVariant;
    }

    @JsonProperty("document-count")
    public String getDocumentCount() {
        return documentCount;
    }

    @JsonProperty("document-count")
    public void setDocumentCount(String documentCount) {
        this.documentCount = documentCount;
    }

    @JsonProperty("subjectArea-area")
    public List<Subject> getSubjectArea() {
        return subjectArea;
    }

    @JsonProperty("subjectArea-area")
    public void setSubjectArea(List<Subject> subjectArea) {
        this.subjectArea = subjectArea;
    }

    @JsonProperty("affiliation-current")
    public AffiliationCurrent getAffiliationCurrent() {
        return affiliationCurrent;
    }

    @JsonProperty("affiliation-current")
    public void setAffiliationCurrent(AffiliationCurrent affiliationCurrent) {
        this.affiliationCurrent = affiliationCurrent;
    }

}
