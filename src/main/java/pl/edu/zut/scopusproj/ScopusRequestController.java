package pl.edu.zut.scopusproj;

import Exceptions.ResultsNotFoundException;
import ResponseORCID.Work.ExternalId;
import ResponseORCID.Work.GetWork;
import Validation.Doi;
import Validation.SingleValidationClass;
import Validation.Titles;
import Validation.ValidationClass;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import getContent.RequestFromExcelData;
import org.apache.poi.ss.usermodel.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;
import pl.edu.zut.scopusproj.authorData.AuthorData;
import pl.edu.zut.scopusproj.forms.InputType;
import pl.edu.zut.scopusproj.worksData.Entry;
import pl.edu.zut.scopusproj.worksData.WorksData;
import singleWorkDetails.AbstractsRetrievalResponse;
import singleWorkDetails.Author_;
import singleWorkDetails.SingleWorkDetails;

import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

@Controller
public class ScopusRequestController {

	private String apiKey = "824dabe59c17805f8ff50ee0e78fd10d";
    public static final String FILE_PATH = "C:\\Users\\BroPro\\Desktop\\ZUT system wymiany danych\\zut-system-wymiany-danych-scopus\\src\\main\\resources\\files\\myFile.xlsx";


	@GetMapping("/")
	public String getResponseFromApiForm(Model model, InputType inputType)
	{
		model.addAttribute("inputType", new InputType());
		return "input_variables";
	}

	@PostMapping("/")
	@ExceptionHandler({ ResultsNotFoundException.class })
	public String getResponseFromApiSubmit(@ModelAttribute InputType inputType, HttpSession session) throws Exception
	{
        return this.getResultsFromExcel(session);

	}

    public String getResultsFromExcel (HttpSession session) throws Exception
    {
        List<RequestFromExcelData> dataFromExcel = this.getFromExcel();
        List<AuthorData> authors = new ArrayList<>();
        List<List<List<AbstractsRetrievalResponse>>>authorWorksList = new ArrayList<>();
        int x = 0;
        for(RequestFromExcelData request : dataFromExcel) {
            AuthorData res = getAuthorsData(request.getName(), request.getSurname(), request.getAffiliation());
                authors.add(res);
            if (!res.getSearchResults().getOpensearchTotalResults().equals("0")) {
                List<List<AbstractsRetrievalResponse>> authorWorks = new ArrayList<>();
                    for (int i = 0; i < res.getSearchResults().getEntry().size(); i++) {
                        if (res.getSearchResults().getEntry().get(i).getDcIdentifier() != null) {
                            String authorId = res.getSearchResults().getEntry().get(i).getDcIdentifier().substring(10);
                            authorWorks.add( getAuthorWorksData(authorId, request.getDateStart(), request.getDateEnd()));
                        }
                    }
                authorWorksList.add(authorWorks);
                x++;
            } else {
                authorWorksList.add(new ArrayList<>());
            }
        }
        List<List<GetWork[]>> orcidWorks = new ArrayList<>();
        List<List<ValidationClass>> validations = new ArrayList<>();
        if (authors.size() > 0) {
            int i = 0;
            for (AuthorData author : authors) {
                if (author.getSearchResults() != null && !author.getSearchResults().getOpensearchTotalResults().equals("0")) {
                    orcidWorks.add(this.getAuthorsOrcidWorks(author, dataFromExcel.get(i).getDateStart(), dataFromExcel.get(i).getDateEnd()));
                }else {
                    orcidWorks.add(new ArrayList<>());
                }
                i++;
            }

                for (int j = 0; j < authors.size(); j++) {
                    if (authors.get(j).getSearchResults() != null && !authors.get(j).getSearchResults().getOpensearchTotalResults().equals("0")) {
                        validations.add(this.validateOrcid(orcidWorks.get(j), authorWorksList.get(j)));
                    } else {
                        validations.add(new ArrayList<>());
                    }
                }
        }
        session.setAttribute("authors", authors);
        session.setAttribute("authorWorks",authorWorksList);
        session.setAttribute("orcidWorks",orcidWorks);
        session.setAttribute("validations", validations);

        return "output_excel_results";
    }

    @PostMapping("/form")
    public String getResultsFromForm (HttpSession session, String name, String surname, String affilation, String dateFrom, String dateTo) throws Exception
    {
        AuthorData res = getAuthorsData(name, surname, affilation);

        if (res.getSearchResults().getOpensearchTotalResults().equals("0")){
            System.out.println("No results found");
            throw new ResultsNotFoundException("No authors found");
        }

        if (res.getSearchResults().getEntry().size() == 0) {
            throw new ResultsNotFoundException("No works for author found");
        }

        LinkedList<AbstractsRetrievalResponse>[] authorWorks = new LinkedList[res.getSearchResults().getEntry().size()];
        for (int i = 0; i < res.getSearchResults().getEntry().size(); i++){
            String authorId = res.getSearchResults().getEntry().get(i).getDcIdentifier().substring(10);
            authorWorks[i] = getAuthorWorksData(authorId, dateFrom, dateTo);
        }

        session.setAttribute("results", res.getSearchResults());
        session.setAttribute("authorWorks",authorWorks);

        return "output_results";
    }

    public List<ValidationClass> validateOrcid(List<GetWork[]> orcidAuthor, List<List<AbstractsRetrievalResponse>> scopusAuthor ) {
        List<ValidationClass> validation = new ArrayList<>();
	    for (int i = 0; i < orcidAuthor.size(); i++) {
	        ValidationClass newValidation = new ValidationClass();
            List<SingleValidationClass> workList = new ArrayList<>();
            newValidation.setSingleValidationClass(workList);
            validation.add(newValidation);
         for (int j = 0; j < orcidAuthor.get(i).length; j++) {
             boolean isValid = false;
             SingleValidationClass singleWorkDetails = new SingleValidationClass();
             Titles titles = new Titles();
             Doi dois = new Doi();
             singleWorkDetails.setTitles(titles);
             singleWorkDetails.setDoi(dois);
             String orcidResultTitle = orcidAuthor.get(i)[j].getBulk().get(0).getWork().getTitle().getTitle().getValue().toLowerCase();
             singleWorkDetails.getTitles().setOrcidTitle(orcidResultTitle);
             String orcidResultDoi = null;
             if (orcidAuthor.get(i)[j].getBulk().get(0).getWork().getExternalIds().getExternalId() != null) {
                 for (ExternalId externalIds : orcidAuthor.get(i)[j].getBulk().get(0).getWork().getExternalIds().getExternalId()) {
                     if (externalIds.getExternalIdType().equals("doi")) {
                         orcidResultDoi = externalIds.getExternalIdValue();
                         singleWorkDetails.getDoi().setOrcidDOI(orcidResultDoi);
                     }
                 }
             }
             if (scopusAuthor.size()  != 0  ) {
                 for (int k = 0; k < scopusAuthor.get(i).size(); k++) {
                     if (orcidResultDoi != null &&
                             scopusAuthor.get(i).get(k).getCoredata().getPrismDoi() != null &&
                             orcidResultDoi.equals(scopusAuthor.get(i).get(k).getCoredata().getPrismDoi())) {

                         isValid = true;
                         if (scopusAuthor.get(i).get(k).getCoredata().getDcTitle() != null) {
                             String scopusResultTitle = scopusAuthor.get(i).get(k).getCoredata().getDcTitle().toLowerCase();
                             singleWorkDetails.getTitles().setScopusTitle(scopusResultTitle);
                         }
                         String scopusResultDoi = scopusAuthor.get(i).get(k).getCoredata().getPrismDoi();
                         singleWorkDetails.getDoi().setScopusDOI(scopusResultDoi);
                         singleWorkDetails.getDoi().setValidationResultDOI("1");

                         if (titles.getOrcidTitle().equals(titles.getScopusTitle())) {
                             singleWorkDetails.getTitles().setValidationResultTitle("1");
                         } else {
                             singleWorkDetails.getTitles().setValidationResultTitle("0");
                         }
                     }
                 }
             }
             if (isValid) {
                 workList.add(singleWorkDetails);
             }
         }
        }
        return validation;
    }

	public List<GetWork[]> getAuthorsOrcidWorks (AuthorData res, String dateFrom, String dateTo) throws Exception {
        GetDataController getDataController = new GetDataController();
        List<GetWork[]> oricdWorks = new ArrayList<GetWork[]>();
        for (int i = 0; i < res.getSearchResults().getEntry().size(); i++){
            Date startDate = new SimpleDateFormat("yyyy-MM-dd").parse(dateFrom);
            Date endDate = new SimpleDateFormat("yyyy-MM-dd").parse(dateTo);
            if ( res.getSearchResults().getEntry().get(i).getOrcid() != null) {
                oricdWorks.add(getDataController.getData(res.getSearchResults().getEntry().get(i).getOrcid(), startDate, endDate));
            }
        }
        return oricdWorks;
    }

    /**
     *
     */
	public List<RequestFromExcelData> getFromExcel() throws IOException, InvalidFormatException
    {
        Workbook workbook = WorkbookFactory.create(new File(FILE_PATH));

        Sheet sheet = workbook.getSheetAt(0);
        DataFormatter dataFormatter = new DataFormatter();

        int i = 1;
        List<RequestFromExcelData> requestsFromExcelData = new ArrayList<RequestFromExcelData>();
        for (Row row: sheet) {
            RequestFromExcelData requestData = new RequestFromExcelData();
            for(Cell cell: row) {
                if (i%5 == 1){
                    requestData.setName(dataFormatter.formatCellValue(cell));
                }
                if (i%5 == 2) {
                    requestData.setSurname(dataFormatter.formatCellValue(cell));
                }
                if (i%5 == 3) {
                    requestData.setAffiliation(dataFormatter.formatCellValue(cell));
                }
                if (i%5 == 4) {
                    requestData.setDateStart(dataFormatter.formatCellValue(cell));
                }
                if (i%5 == 0) {
                    requestData.setDateEnd(dataFormatter.formatCellValue(cell));
                    requestsFromExcelData.add(requestData);
                }

                String cellValue = dataFormatter.formatCellValue(cell);
                System.out.print(cellValue + "\t");
                i++;
            }
            System.out.println();
        }
        return requestsFromExcelData;
    }
	public UriComponents getUriComponents(MultiValueMap<String, String> queryParams, String path)
	{
		UriComponents uriComponents = UriComponentsBuilder.newInstance()
				.scheme("https").host("api.elsevier.com").path(path)
				.queryParams(queryParams)
				.build(false);
		System.out.println(uriComponents.encode().toUri());
		return uriComponents;
	}

	public AuthorData getAuthorsData(String name, String surname, String affiliation)
	{
		String query = "";
		if (name != null) {
			query = query + "AUTHFIRST(" + name + ")";
		} if(surname != null) {
			query = query + "AUTHLASTNAME(" + surname + ")";
		} if(affiliation != null) {
			String afil = affiliation.replaceAll("\\s+","+");
			query = query + "AFFIL(" + afil + ")";
		}
			MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<>();
			queryParams.add("apiKey", apiKey);
			queryParams.add("query", query);
			UriComponents uriComponents = getUriComponents(queryParams, "/content/search/author");
			return new RestTemplate().getForObject(uriComponents.toUriString(), AuthorData.class);
	}

    public LinkedList<AbstractsRetrievalResponse> getAuthorWorksData(String authorId)
    {
       return this.getAuthorWorksData(authorId, null, null);
    }

	public LinkedList<AbstractsRetrievalResponse> getAuthorWorksData(String authorId, String dateStart, String dateEnd)
	{
        String query = "AU-ID(" + authorId + ")";
        if (dateStart != null && dateEnd != null) {
            query += " AND PUBYEAR > " +  dateStart.substring(0,4) + " AND PUBYEAR < " + dateEnd.substring(0,4);
        }
		MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<>();
		queryParams.add("apiKey", apiKey);
		queryParams.add("query", query);
		queryParams.add("field", "dc:identifier");

		UriComponents uriComponents = getUriComponents(queryParams, "/content/search/scopus");
		WorksData worksresult = new RestTemplate().getForObject(uriComponents.toUriString(), WorksData.class);
		LinkedList<AbstractsRetrievalResponse> authorWorks = new LinkedList<>();
		queryParams.clear();
        if (worksresult != null && !worksresult.getSearchResults().getOpensearchTotalResults().equals("0")) {
            for (Entry work : worksresult.getSearchResults().getEntry()) {
                queryParams.add("apiKey", apiKey);
                queryParams.add("field", "authors,title,publicationName,volume,issueIdentifier,prism:pageRange,coverDate,article-number,doi,citedby-count,prism:aggregationType");
                String workId = work.getDcIdentifier().substring(10);
                uriComponents = getUriComponents(queryParams, "content/abstract/scopus_id/" + workId);
                SingleWorkDetails singleWorkDetails = new RestTemplate().getForObject(uriComponents.toUriString(), SingleWorkDetails.class);
                queryParams.clear();
                authorWorks.add(singleWorkDetails.getAbstractsRetrievalResponse());
                for (Author_ author: authorWorks.getLast().getAuthors().getAuthor())
                {
                    author.setAdditionalProperty("affiliationName", "");
                }
            }
		}
		return authorWorks;
	}
}
