
package pl.edu.zut.scopusproj.abstractdata;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "@abbrev",
    "@frequency",
    "$"
})
public class SubjectArea {

    @JsonProperty("@abbrev")
    private String abbrev;
    @JsonProperty("@frequency")
    private String frequency;
    @JsonProperty("$")
    private String $;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("@abbrev")
    public String getAbbrev() {
        return abbrev;
    }

    @JsonProperty("@abbrev")
    public void setAbbrev(String abbrev) {
        this.abbrev = abbrev;
    }

    @JsonProperty("@frequency")
    public String getFrequency() {
        return frequency;
    }

    @JsonProperty("@frequency")
    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    @JsonProperty("$")
    public String get$() {
        return $;
    }

    @JsonProperty("$")
    public void set$(String $) {
        this.$ = $;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
