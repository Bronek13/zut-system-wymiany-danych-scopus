
package pl.edu.zut.scopusproj.abstractdata;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "@_fa",
    "link",
    "prism:url",
    "dc:identifier",
    "eid",
    "orcid",
    "preferred-name",
    "name-variant",
    "document-count",
    "subject-area",
    "affiliation-current"
})
public class Entry {

    @JsonProperty("@_fa")
    private String fa;
    @JsonProperty("link")
    private List<Link_> link = null;
    @JsonProperty("prism:url")
    private String prismUrl;
    @JsonProperty("dc:identifier")
    private String dcIdentifier;
    @JsonProperty("eid")
    private String eid;
    @JsonProperty("orcid")
    private String orcid;
    @JsonProperty("preferred-name")
    private PreferredName preferredName;
    @JsonProperty("name-variant")
    private List<NameVariant> nameVariant = null;
    @JsonProperty("document-count")
    private String documentCount;
    @JsonProperty("subject-area")
    private List<SubjectArea> subjectArea = null;
    @JsonProperty("affiliation-current")
    private AffiliationCurrent affiliationCurrent;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("@_fa")
    public String getFa() {
        return fa;
    }

    @JsonProperty("@_fa")
    public void setFa(String fa) {
        this.fa = fa;
    }

    @JsonProperty("link")
    public List<Link_> getLink() {
        return link;
    }

    @JsonProperty("link")
    public void setLink(List<Link_> link) {
        this.link = link;
    }

    @JsonProperty("prism:url")
    public String getPrismUrl() {
        return prismUrl;
    }

    @JsonProperty("prism:url")
    public void setPrismUrl(String prismUrl) {
        this.prismUrl = prismUrl;
    }

    @JsonProperty("dc:identifier")
    public String getDcIdentifier() {
        return dcIdentifier;
    }

    @JsonProperty("dc:identifier")
    public void setDcIdentifier(String dcIdentifier) {
        this.dcIdentifier = dcIdentifier;
    }

    @JsonProperty("eid")
    public String getEid() {
        return eid;
    }

    @JsonProperty("eid")
    public void setEid(String eid) {
        this.eid = eid;
    }

    @JsonProperty("orcid")
    public String getOrcid() {
        return orcid;
    }

    @JsonProperty("orcid")
    public void setOrcid(String orcid) {
        this.orcid = orcid;
    }

    @JsonProperty("preferred-name")
    public PreferredName getPreferredName() {
        return preferredName;
    }

    @JsonProperty("preferred-name")
    public void setPreferredName(PreferredName preferredName) {
        this.preferredName = preferredName;
    }

    @JsonProperty("name-variant")
    public List<NameVariant> getNameVariant() {
        return nameVariant;
    }

    @JsonProperty("name-variant")
    public void setNameVariant(List<NameVariant> nameVariant) {
        this.nameVariant = nameVariant;
    }

    @JsonProperty("document-count")
    public String getDocumentCount() {
        return documentCount;
    }

    @JsonProperty("document-count")
    public void setDocumentCount(String documentCount) {
        this.documentCount = documentCount;
    }

    @JsonProperty("subject-area")
    public List<SubjectArea> getSubjectArea() {
        return subjectArea;
    }

    @JsonProperty("subject-area")
    public void setSubjectArea(List<SubjectArea> subjectArea) {
        this.subjectArea = subjectArea;
    }

    @JsonProperty("affiliation-current")
    public AffiliationCurrent getAffiliationCurrent() {
        return affiliationCurrent;
    }

    @JsonProperty("affiliation-current")
    public void setAffiliationCurrent(AffiliationCurrent affiliationCurrent) {
        this.affiliationCurrent = affiliationCurrent;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
